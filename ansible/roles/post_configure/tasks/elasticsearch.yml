- name: Configure License
  include_tasks: license.yml
  when: gitlab_license_plan is not defined
  tags: license

- name: Clean out previous Elasticsearch configuration and index
  block:
    - name: Delete old Elasticsearch index
      block:
        - name: Delete old Elasticsearch index for GitLab via GitLab Rails
          command: "{{ item }}"
          loop:
            - gitlab-rake gitlab:elastic:delete_index
            - gitlab-rake gitlab:elastic:clear_index_status
          delegate_to: "{{ groups['gitlab_rails'][0] }}"
          delegate_facts: true
          become: true
          register: delete_result
          failed_when:
            - delete_result.rc != 0
            - '"Net::OpenTimeout" not in delete_result.stderr'
          when: "'gitlab_rails' in groups"

        - name: Delete old Elasticsearch index for GitLab via GitLab Toolbox pod
          kubernetes.core.k8s_exec:
            pod: "{{ toolbox_pod }}"
            namespace: "{{ gitlab_charts_release_namespace }}"
            command: "{{ item }}"
          loop:
            - gitlab-rake gitlab:elastic:delete_index
            - gitlab-rake gitlab:elastic:clear_index_status
          register: delete_result
          failed_when:
            - delete_result.rc != 0
            - '"Net::OpenTimeout" not in delete_result.stderr'
          when:
            - toolbox_pod is defined
            - "'gitlab_rails' not in groups"

    - name: Unconfigure Elasticsearch settings
      block:
        - name: Unconfigure Elasticsearch settings via GitLab Rails
          command: |
            gitlab-rails runner "
              ApplicationSetting.last.update(elasticsearch_url: '')
              ApplicationSetting.last.update(elasticsearch_indexing: false)
              ApplicationSetting.last.update(elasticsearch_search: false)
            "
          delegate_to: "{{ groups['gitlab_rails'][0] }}"
          delegate_facts: true
          become: true
          when: "'gitlab_rails' in groups"

        - name: Unconfigure Elasticsearch settings via GitLab Toolbox pod
          kubernetes.core.k8s_exec:
            pod: "{{ toolbox_pod }}"
            namespace: "{{ gitlab_charts_release_namespace }}"
            command: |
              gitlab-rails runner "
                ApplicationSetting.last.update(elasticsearch_url: '')
                ApplicationSetting.last.update(elasticsearch_indexing: false)
                ApplicationSetting.last.update(elasticsearch_search: false)
              "
          when:
            - toolbox_pod is defined
            - "'gitlab_rails' not in groups"
      when: gitlab_license_plan in ['premium', 'ultimate']

    - name: Pause for 60 secs for old Elasticsearch index to clear
      wait_for:
        timeout: 60
  when:
    - gitlab_license_plan in ['premium', 'ultimate']
    - elasticsearch_clean_install | bool

- name: Get and save Environment Settings
  block:
    - name: Get and save Environment Settings via GitLab Rails
      command: "gitlab-rails runner 'print ApplicationSetting.last.attributes.to_json'"
      delegate_to: "{{ groups['gitlab_rails'][0] }}"
      delegate_facts: true
      become: true
      register: env_settings_response_rails
      when: "'gitlab_rails' in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_rails.stdout | from_json }}'
      when: "'gitlab_rails' in groups"

    - name: Get and save Environment Settings via GitLab Toolbox pod
      kubernetes.core.k8s_exec:
        pod: "{{ toolbox_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: "gitlab-rails runner 'print ApplicationSetting.last.attributes.to_json'"
      register: env_settings_response_pod
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_pod.stdout | from_json }}'
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"
  when: gitlab_license_plan in ['premium', 'ultimate']

- name: Configure Elasticsearch Indexing and URL setting
  block:
    - name: Enable GitLab Elasticsearch indexing setting via GitLab Rails
      command: |
        gitlab-rails runner "
          ApplicationSetting.last.update(elasticsearch_url: \"{{ advanced_search_hosts | join(',') }}\")
          ApplicationSetting.last.update(elasticsearch_indexing: true)
        "
      delegate_to: "{{ groups['gitlab_rails'][0] }}"
      delegate_facts: true
      become: true
      register: elasticsearch_status_result
      retries: 5
      delay: 5
      until: elasticsearch_status_result is success
      when: "'gitlab_rails' in groups"

    - name: Enable GitLab Elasticsearch indexing setting via GitLab Toolbox pod
      kubernetes.core.k8s_exec:
        pod: "{{ toolbox_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: |
          gitlab-rails runner "
            ApplicationSetting.last.update(elasticsearch_url: \"{{ advanced_search_hosts | join(',') }}\")
            ApplicationSetting.last.update(elasticsearch_indexing: true)
          "
      register: elasticsearch_status_result
      retries: 5
      delay: 5
      until: elasticsearch_status_result is success
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"

    - name: Pause for 60 secs to allow Sidekiq caches to be updated
      wait_for:
        timeout: 60
  when:
    - gitlab_license_plan in ['premium', 'ultimate']
    - gitlab_environment_settings.elasticsearch_url != (advanced_search_hosts | join(',')) or not gitlab_environment_settings.elasticsearch_indexing

- name: Perform Elasticsearch Indexing
  block:
    - name: Create empty index for GitLab
      block:
        - name: Create empty index for GitLab via GitLab Rails
          command: gitlab-rake gitlab:elastic:create_empty_index
          delegate_to: "{{ groups['gitlab_rails'][0] }}"
          delegate_facts: true
          become: true
          when: "'gitlab_rails' in groups"

        - name: Create empty index for GitLab via GitLab Toolbox pod
          kubernetes.core.k8s_exec:
            pod: "{{ toolbox_pod }}"
            namespace: "{{ gitlab_charts_release_namespace }}"
            command: gitlab-rake gitlab:elastic:create_empty_index
          when:
            - toolbox_pod is defined
            - "'gitlab_rails' not in groups"

    - name: Index all GitLab projects
      block:
        - name: Index all GitLab projects via GitLab Rails
          command: gitlab-rake gitlab:elastic:index_projects
          delegate_to: "{{ groups['gitlab_rails'][0] }}"
          delegate_facts: true
          become: true
          when: "'gitlab_rails' in groups"

        - name: Index all GitLab projects via GitLab Toolbox pod
          kubernetes.core.k8s_exec:
            pod: "{{ toolbox_pod }}"
            namespace: "{{ gitlab_charts_release_namespace }}"
            command: gitlab-rake gitlab:elastic:index_projects
          when:
            - toolbox_pod is defined
            - "'gitlab_rails' not in groups"

    - name: Wait until indexing is complete
      block:
        - name: Wait until indexing is complete via GitLab Rails
          command: gitlab-rake gitlab:elastic:index_projects_status
          delegate_to: "{{ groups['gitlab_rails'][0] }}"
          delegate_facts: true
          become: true
          register: index_status_result
          retries: 20
          delay: 10
          until: "(index_status_result.stdout | regex_search('[0-9]+\\.[0-9]+') | float > 90.0) or '(0/0 projects)' in index_status_result.stdout"
          when: "'gitlab_rails' in groups"

        - name: Wait until indexing is complete via GitLab Toolbox pod
          kubernetes.core.k8s_exec:
            pod: "{{ toolbox_pod }}"
            namespace: "{{ gitlab_charts_release_namespace }}"
            command: gitlab-rake gitlab:elastic:index_projects_status
          register: index_status_result
          retries: 20
          delay: 10
          until: "(index_status_result.stdout | regex_search('[0-9]+\\.[0-9]+') | float > 90.0) or '(0/0 projects)' in index_status_result.stdout"
          when:
            - toolbox_pod is defined
            - "'gitlab_rails' not in groups"
  when:
    - gitlab_license_plan in ['premium', 'ultimate']
    - not gitlab_environment_settings.elasticsearch_indexing

- name: Get and save Environment Settings
  block:
    - name: Get and save Environment Settings via GitLab Rails
      command: "gitlab-rails runner 'print ApplicationSetting.last.attributes.to_json'"
      delegate_to: "{{ groups['gitlab_rails'][0] }}"
      delegate_facts: true
      become: true
      register: env_settings_response
      when: "'gitlab_rails' in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_rails.stdout | from_json }}'
      when: "'gitlab_rails' in groups"

    - name: Get and save Environment Settings via GitLab Toolbox pod
      kubernetes.core.k8s_exec:
        pod: "{{ toolbox_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: "gitlab-rails runner 'print ApplicationSetting.last.attributes.to_json'"
      register: env_settings_response_pod
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"

    - name: Save Environment Settings
      set_fact:
        gitlab_environment_settings: '{{ env_settings_response_pod.stdout | from_json }}'
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"
  when: gitlab_license_plan in ['premium', 'ultimate']

- name: Enable Elasticsearch Search
  block:
    - name: Enable Elasticsearch Search via GitLab Rails
      command: "gitlab-rails runner 'ApplicationSetting.last.update(elasticsearch_search: true)'"
      delegate_to: "{{ groups['gitlab_rails'][0] }}"
      delegate_facts: true
      become: true
      when: "'gitlab_rails' in groups"

    - name: Enable Elasticsearch Search via GitLab Toolbox pod
      kubernetes.core.k8s_exec:
        pod: "{{ toolbox_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: "gitlab-rails runner 'ApplicationSetting.last.update(elasticsearch_search: true)'"
      when:
        - toolbox_pod is defined
        - "'gitlab_rails' not in groups"
  when:
    - gitlab_license_plan in ['premium', 'ultimate']
    - not gitlab_environment_settings.elasticsearch_search
